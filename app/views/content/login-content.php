<div class="container">
    <div class="row">
        <div class="col-12 text-center mt-5 mb-4">
            <h4>Login</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 m-auto">
            <form id="login-form" method="POST" action="login/process">
                <div class="form-group">
                    <label>Email<span>*</span></label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                    <div class="error pt-1"></div>
                </div>
                <div class="form-group">
                    <label>Password<span>*</span></label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                    <div class="error pt-1"></div>
                </div>
                <button type="submit" class="col-12 btn btn-primary mt-2">Login</button>
            </form>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 col-md-6 col-lg-4 m-auto text-center">
            <p class="register">Do not have an account?
                <a href="<?php echo htmlspecialchars('register'); ?>">Register</a>
            </p>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-12 text-center">
            <p><small>Required fields are marked with <span>*</span></small></p>
        </div>
    </div>
</div>

