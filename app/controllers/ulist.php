<?php

require_once '../app/models/User.php';

class Ulist extends Controller {

    public function index() {

        $user = $this->model('User');

        $users = $user->fetchAll();


        $this->view('ulist/index', [
            'users' => $users
        ]);
    }

}
