<?php

session_start();

if (!empty($_SESSION['logged_in_user'])) {

    header('Location: /backoffice');

    
} 

require_once '../app/views/layout/header.php';

require_once '../app/views/content/login-content.php';

require_once '../app/views/layout/footer.php';

