<div class="container">
    <div class="row">
        <div class="col-12 text-center mt-5 mb-4">
            <h4>Create an account</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 m-auto">
            <form id="registration-form" method="POST" action="<?php echo htmlspecialchars('/register/process') ?>">
                <input type="hidden" name="task" value="register">
                <div class="form-group">
                    <label>Name<span>*</span></label>
                    <input type="text" name="username" class="form-control" id="name" placeholder="Enter name">
                    <div class="error pt-1"></div>
                </div>
                <div class="form-group">
                    <label>Email<span>*</span></label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                    <div class="error pt-1"></div>
                </div>
                <div class="form-group">
                    <label>Password<span>*</span></label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                    <div class="error pt-1"></div>
                </div>
                <button type="submit" class="col-12 btn btn-primary mt-2">Register</button>
            </form>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 col-md-6 col-lg-4 m-auto text-center">
            <p class="register">Already have account?
                <a href="<?php echo htmlspecialchars('/') ?>">Login</a>
            </p>
        </div>
    </div>
    <div class="row mt-2 mb-4 mb-lg-0">
        <div class="col-12 text-center">
            <p><small>Required fields are marked with <span>*</span></small></p>
        </div>
    </div>
</div>

