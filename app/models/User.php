<?php

require_once '../app/core/DB.php';

class User {

    /**
     * @return array Array of associative arrays that represent rows
     */
    public function fetchAll() {

        $link = DB::getInstance()->connect();

        $query = "SELECT * FROM users ORDER BY created_at DESC";
        
        $result = $link->query($query);

        if ($result->num_rows > 0) {

           return $result;
            
        } else {
            
            echo "Error: " . $query . "<br>" . $link->error;
            
        }
        
        $result->free();

        $link->close();

        
    }

    /**
     * @param int $id
     * @return array Associative array that represent one row
     */
    public function getUserById($id) {

        $link = DB::getInstance()->connect();

        $escpdId = $link->real_escape_string($id);

        $query = "SELECT * FROM users WHERE id='$escpdId'";

        $result = $link->query($query);

        if ($result->num_rows == 1) {

            $row = $result->fetch_assoc();
        }

        $result->free();

        $link->close();

        return $row;
    }

    /**
     * @param string $email
     * @return array Associative array that represent one row
     */
    public function findUserByEmail($email) {

        $link = DB::getInstance()->connect();

        $escpdEmail = $link->real_escape_string($email);

        $query = "SELECT * FROM users "
                . "WHERE email='$escpdEmail'";


        $result = $link->query($query);


        if ($result->num_rows == 1) {

            $row = $result->fetch_assoc();
        }

        $result->free();

        $link->close();

        return $row;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @return int Id of last insert user
     */
    public function insert($username, $email, $password) {

        $createdAt = date('Y-m-d H:i:s');

        $pass = md5($password);

        $link = DB::getInstance()->connect();

        $escpdUsername = $link->real_escape_string($username);
        $escpdEmail = $link->real_escape_string($email);
        $escpdPassword = $link->real_escape_string($pass);
        $escpdJoined = $link->real_escape_string($createdAt);

        $query = "INSERT INTO users (username, email, password, created_at) "
                . "VALUES ('$escpdUsername',"
                . " '$escpdEmail',"
                . " '$escpdPassword',"
                . " '$escpdJoined')";

        $result = $link->query($query);

        if ($result === TRUE) {

            $lastId = $link->insert_id;
        }

        $link->close();

        return $lastId;
    }

    /**
     * @param string $email
     * @param string $password
     * @return array User row if email and password are ok, FALSE otherwise
     */
    public function checkCredentials($email, $password) {

        $user = self::findUserByEmail($email);

        $pass = md5($password);

        if (!empty($user)) {
            if ($pass == $user['password']) {

                return $user;
            }
        }

        return false;
    }

}
