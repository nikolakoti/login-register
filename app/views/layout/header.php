<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Nikola Kotarac">
        <meta name="description" content="index page">
        <meta name="keywords" content="">
        <title></title>



        <link rel="apple-touch-icon" sizes="180x180" href="/skins/front/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/skins/front/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/skins/front/favicon/favicon-16x16.png">
        <link rel="manifest" href="/skins/front/favicon/site.webmanifest">
        <link rel="mask-icon" href="/skins/front/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">



        <!--FONTS CSS-->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 



        <!-- Bootstrap -->
        <link href="/skins/front/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!--THEME CSS-->
        <link href="/skins/front/css/style.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

