
$(document).ready(function () {

    var logoutForm = $('#exampleModal #logout-form');

    var button = $('#exampleModal button.btn-primary');

    button.on('click', function () {

        logoutForm.submit();
    });


    $("#login-form").validate({
        highlight: function (element) {
            $(element).closest('.form-group').addClass("has-danger");
            $(element).addClass("contact-form-danger");
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-danger');

        },
        rules: {
            email: {
                required: true,
                email: true
            },

            password: {
                required: true

            }

        },
        messages: {
            email: {
                required: "Email field is required!",
                email: "Enter valid email adress!"
            },

            password: {
                required: "Password field is required!"

            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            error.appendTo($(element).closest('.form-group').find('.error'));
        }
    });

    $("#registration-form").validate({
        highlight: function (element) {
            $(element).closest('.form-group').addClass("has-danger");
            $(element).addClass("contact-form-danger");
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-danger');

        },
        rules: {

            username: {
                required: true,
                rangelength: [2, 20]

            },

            email: {
                required: true,
                email: true
            },

            password: {
                required: true,
                rangelength: [5, 20]

            }

        },
        messages: {

            username: {

                required: "Name field is required!",
                rangelength: "Name must be beetween 2 and 20 chars long!"

            },
            email: {
                required: "Email field is required!",
                email: "Enter valid email adress!"
            },

            password: {
                required: "Password field is required!",
                rangelength: "Password must be beetween 5 and 20 chars long!"


            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            error.appendTo($(element).closest('.form-group').find('.error'));
        }
    });


    
    var titleTag = $('html title');

    var paths = ['/', '/register', '/backoffice', '/ulist'];

    var titles = ['Login to BO', 'Create an account', 'BO - Welcome', 'BO - User list'];

    $(titles).each(function (index) {

        if (window.location.pathname === paths[index]) {

            $(titleTag).append(titles[index]);
        }

    });


});


