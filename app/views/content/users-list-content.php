<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand" href="<?php echo htmlspecialchars('backoffice') ?>">BO</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo htmlspecialchars('backoffice') ?>">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo htmlspecialchars('ulist') ?>">List all users</a>
            </li>
            <li class="nav-item">
                <a id="modal-trigger" data-toggle="modal" data-target="#exampleModal" class="nav-link" href="#">Logout</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <table class="table table-dark mt-5">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Registered</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['users'] as $user) { ?>
                <tr>
                    <th scope="row"><?php echo htmlspecialchars($user['id']) ?></th>
                    <td><?php echo htmlspecialchars($user['username']) ?></td>
                    <td><?php echo htmlspecialchars($user['email']) ?></td>
                    <td><?php echo htmlspecialchars($user['created_at']) ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to logout?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Logout</button>
                <form id="logout-form" 
                      method="POST" 
                      action="backoffice/logout" 
                      style="display: none;"
                      >
                </form>
            </div>
        </div>
    </div>
</div>



