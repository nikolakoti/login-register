<?php

require_once '../app/core/DB.php';

class Login extends Controller {

    public function index() {

        $this->view('login/index');
    }

    public function process() {


        if ($_SERVER['REQUEST_METHOD'] === 'POST') {


            $formaData = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password'])
            ];
            
            $user = $this->model('User');


            if (!empty($user->checkCredentials($formaData['email'], $formaData['password']))) {

                session_start();

                $_SESSION['logged_in_user'] = $user->checkCredentials($formaData['email'], $formaData['password']);

                header('Location: /backoffice');
            } else {

                echo '<div>'
                . '<p style="color:orangered;">Make sure you enter email or password correctly</p>'
                . '<a href="/">Back to login page</a>'
                . '</div>';
            }
        }
    }

}
