-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 17, 2018 at 03:14 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diwanee`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` char(40) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created_at`, `updated_at`) VALUES
(6, 'Jay Jay', 'jj@gmail.com', 'b87c1c635c35b74380c0813b8dd35a5c', '2018-09-13 21:57:30', '2018-09-13 21:57:30'),
(8, 'Marko Markovic', 'mmarko@gmail.com', 'c28aa76990994587b0e907683792297c', '2018-09-14 22:13:40', NULL),
(10, 'Nemanja Nikolic', 'nn@gmail.com', '0d4e3eb97b434fce188ce85215f875db', '2018-09-14 23:23:39', NULL),
(12, 'Mila Milovanovic', 'mila@gmail.com', 'f562f7f28a039094f7b602c033f106a4', '2018-09-15 10:09:28', NULL),
(17, 'Milan Milunovic', 'milan.milunov@test.com', '83227a721a3363d2c78381664c78657f', '2018-09-15 20:25:57', NULL),
(19, 'Jovan Jovanovic', 'jovan@test.com', 'b59c6e9b344bae1a36fe427a42889265', '2018-09-15 20:37:15', NULL),
(43, 'Djordje Matic', 'djmatic@gmail.com', 'c96f8b148c5f4e4ce298bb8fde65a56f', '2018-09-15 23:27:40', NULL),
(44, 'Mark Strong', 'mk@test.com', '6f7f9432d35dea629c8384dab312259a', '2018-09-16 11:07:40', NULL),
(46, 'Jimmy Hendrix', 'jhendrix@rip.com', 'b1c84f8d672b5d6a84a7a486e81b465a', '2018-09-16 12:19:05', NULL),
(47, 'Nikola Kotarac', 'nikola.kotarac@yahoo.com', '44eae4f787750aa96e155dad34d7352d', '2018-09-16 14:20:38', NULL),
(48, 'Zuman', 'zuman@gmail.com', 'b85d82822df39909366a71c78feb7295', '2018-09-16 21:58:20', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
