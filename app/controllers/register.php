<?php

require_once '../app/core/DB.php';

class Register extends Controller {

    public function index() {

        $this->view('register/index');
    }

    public function process() {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            if (isset($_POST["task"]) && $_POST["task"] == "register") {

                $formaData = [
                    'username' => trim($_POST['username']),
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password'])
                ];

                $user = $this->model('User');

                $lastInsertedId = $user->insert($formaData['username'], $formaData['email'], $formaData['password']);


                $newUser = $user->getUserById($lastInsertedId);

                if (isset($newUser)) {

                    session_start();

                    $_SESSION['logged_in_user'] = $newUser;

                    header('Location: /backoffice');
                } else {

                    header('Location: /');
                }
            }
        }
    }

}
