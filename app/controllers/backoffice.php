<?php

class BackOffice extends Controller {
    
    public function index() {
        
        $this->view('backoffice/index');
    }
    
    public function logout() {
        
        session_start();
        
        
        session_destroy();
        
        header('Location: /');
    }

}

