<?php

session_start();

if (empty($_SESSION['logged_in_user'])) {

    header('Location: /');
    die();
}



require_once '../app/views/layout/header.php';

require_once '../app/views/content/backoffice-content.php';

require_once '../app/views/layout/footer.php';

